namespace MovieAppi.Migrations
{
    using MovieAppi.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MovieAppi.Models.MovieAppiContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MovieAppi.Models.MovieAppiContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            
            context.Directors.AddOrUpdate(x => x.Id,
                new Director() { Id = 1, Name = "Kenneth", LastName = "Lonergan", Age = 56 },
                new Director() { Id = 2, Name = "Sam", LastName = "Mendes", Age = 53 },
                new Director() { Id = 3, Name = "Todd", LastName = "Haynes", Age = 57 },
                new Director() { Id = 4, Name = "Fernando", LastName = "Meirelles", Age = 63 }
                );

            context.Movies.AddOrUpdate(x => x.Id,
                new Movie() { Id = 1, Name = "Manchester by the Sea", Genre = "Drama", Year = 2016, DirectorId = 1 },
                new Movie() { Id = 2, Name = "You Can Count on Me", Genre = "Drama", Year = 2000, DirectorId = 1 },
                new Movie() { Id = 3, Name = "American Beauty", Genre = "Drama", Year = 1999, DirectorId = 2 },
                new Movie() { Id = 4, Name = "Keepers of the Magic", Genre = "Documentary", Year = 2016, DirectorId = 2 },
                new Movie() { Id = 5, Name = "Carol", Genre = "Drama, Romance", Year = 2015, DirectorId = 3 },
                new Movie() { Id = 6, Name = "Buoy", Genre = "Drama", Year = 2012, DirectorId = 3 },
                new Movie() { Id = 7, Name = "City of God", Genre = "Crime, Drama", Year = 2002, DirectorId = 4 },
                new Movie() { Id = 8, Name = "A Lei da �gua (Novo C�digo Florestal)", Genre = "Documentary, History", Year = 2015, DirectorId = 4 }
            );
        }
    }
}
