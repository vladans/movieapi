﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MovieAppi.Models
{
    public class MovieAppiContext : DbContext
    {
        public MovieAppiContext() : base("name=MovieAppiContext")
        {
                
        }

        public DbSet<Director> Directors { get; set; }
        public DbSet<Movie> Movies { get; set; }

    }
}