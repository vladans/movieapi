﻿using MovieAppi.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Tracing;

namespace MovieAppi.Controllers
{
    public class DirectorsController : ApiController
    {
        private MovieAppiContext db = new MovieAppiContext();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET api/directors
        public IQueryable<Director> GetDirectors()
        {
            Configuration.Services.GetTraceWriter().Info(
                Request,
                "DirectorsController",
                "Get all directors"
            );
            return db.Directors;
        }

        // GET api/directors/1
        [ResponseType(typeof(Director))]
        public IHttpActionResult GetDirector(int id)
        {
            Director director = db.Directors.Find(id);
            if (director == null)
            {
                return NotFound();
            }
            return Ok(director);
        }

        // POST api/directors
        [ResponseType(typeof(Director))]
        public IHttpActionResult PostDirector(Director director)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Directors.Add(director);
            db.SaveChanges();
            return CreatedAtRoute("DefaultApi", new { id = director.Id }, director);
        }

        // PUT api/directors/1
        [ResponseType(typeof(Director))]
        public IHttpActionResult PutDirector(int id, Director director)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != director.Id)
            {
                return BadRequest();
            }
            db.Entry(director).State = EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                bool directorExist = db.Directors.Count(x => x.Id == id) > 0;
                if (!directorExist)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(director);
        }

        // DELETE api/directors/1
        [ResponseType(typeof(Director))]
        public IHttpActionResult DeleteDirector(int id)
        {
            Director director = db.Directors.Find(id);
            if (director == null)
            {
                return NotFound();
            }
            db.Directors.Remove(director);
            db.SaveChanges();
            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}
