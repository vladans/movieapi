﻿using MovieAppi.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Tracing;

namespace MovieAppi.Controllers
{
    public class MoviesController : ApiController
    {
        private MovieAppiContext db = new MovieAppiContext();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET api/movies
        public IQueryable<Movie> GetMovies()
        {
            Configuration.Services.GetTraceWriter().Info(
                Request,
                "MoviesController",
                "Get all movies"
            );
            return db.Movies.Include(x => x.Director);
        }

        // GET api/movies/1
        [ResponseType(typeof(Movie))]
        public IHttpActionResult GetMovie(int id)
        {
            var movie = db.Movies.Include(x => x.Director).FirstOrDefault(x => x.Id == id);
            if (movie == null)
            {
                return NotFound();
            }
            return Ok(movie);
        }

        // POST api/movie
        [ResponseType(typeof(Movie))]
        public IHttpActionResult PostMovie(Movie movie)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Movies.Add(movie);
            db.SaveChanges();
            return CreatedAtRoute("DefaultApi", new { id = movie.Id }, movie);
        }

        // PUT api/movies/1
        [ResponseType(typeof(Movie))]
        public IHttpActionResult PutMovie(int id, Movie movie)
        {
            movie.Director = db.Directors.Find(movie.DirectorId);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if ( id != movie.Id)
            {
                return BadRequest();
            }
            db.Entry(movie).State = EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                bool movieExist = db.Movies.Count(x => x.Id == id) > 0;
                if (!movieExist)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(movie);
        }

        //DELETE api/movies/a
        [ResponseType(typeof(Movie))]
        public IHttpActionResult DeleteMovie(int id)
        {
            Movie movie = db.Movies.Find(id);
            if (movie == null)
            {
                return NotFound();
            }
            db.Movies.Remove(movie);
            db.SaveChanges();
            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}
